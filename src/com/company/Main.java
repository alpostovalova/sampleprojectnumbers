package com.company;

import java.io.BufferedReader;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) {

        String[] array = ReadingfromFile().split(",");
        int[] numbers = new int[20];

        for (int i =0; i<array.length; i++){
            numbers[i] = Integer.parseInt(array[i]);
        }

        String message = "Сортировка по возрастанию: ";
        boolean flag = true;
        int j = 0;
        int tmp;
        while (flag) {
            flag = false;
            j++;
            for (int i = 0; i < numbers.length - j; i++) {
                if (numbers[i] > numbers[i + 1]) {
                    tmp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = tmp;
                    flag = true;
                }
            }
        }

        printLnArray(numbers, message);

        message = "Сортировка по убыванию: ";
        flag = true;
        j = 0;
        tmp = 0;
        while (flag) {
            flag = false;
            j++;
            for (int i = 0; i < numbers.length - j; i++) {
                if (numbers[i] < numbers[i + 1]) {
                    tmp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = tmp;
                    flag = true;
                }
            }
        }

        printLnArray(numbers, message);

    }

    public static String ReadingfromFile() {
        String data = "";

        try {
            FileReader fr = new FileReader("input.txt");
            BufferedReader br = new BufferedReader(fr);
            data = br.readLine();

        } catch (Exception e) {
            System.out.println(e);
        }
        return data;
    }

    public static void printLnArray(int[] numbers, String message){
        System.out.println("\n" + message);
        for (int i=0; i < numbers.length; i++){
            System.out.print(String.valueOf( numbers[i]) + " ");
        }
    }
}
